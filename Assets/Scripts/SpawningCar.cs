﻿using UnityEngine;
using System.Collections;

public class SpawningCar : MonoBehaviour {

	// Get the enemy car game object. Saved as a prefab in the asset folder
	public GameObject[] cars;
	 int carNo;
	// horizontal size for the scene for random car movement
	private float maxHorizontalSizeForCar = 2.5f;

	//spwan the car object after every 1 second
	float delayTime = 1.6f;
	float timer;

	// Use this for initialization
	void Start () {
		timer = delayTime;




	}
	
	// Update is called once per frame
	void Update () {

		timer -= Time.deltaTime; 

		if (timer <= 0) {
			
			// create random position for the car game object 
			// y and z axis remain same as we declare on the unity
			
			Vector3 carRandomPos = new Vector3 (Random.Range(-maxHorizontalSizeForCar, maxHorizontalSizeForCar), transform.position.y
			                                    , transform.position.z);
			// create the clone of enemy car game object
			
			// first sending game object, then vector3 position of the car object, then rotation
			
			// rotation remain same as we declare at the unity

			 carNo = Random.Range(0,3);
			
			Instantiate (cars[carNo], carRandomPos, transform.rotation);

			// after being zero we must have to initialize the value for next spawning of the car
			timer = delayTime;
		}

	}
}
