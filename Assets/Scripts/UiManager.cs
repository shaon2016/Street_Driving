﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class UiManager : MonoBehaviour {

	// array of button for controlling the button ui
	public Button[] buttons;


	public Text scoreText;
	int scoreNo;
	bool gameOver;
	// Use this for initialization
	void Start () {
		scoreNo = 0;
		gameOver = false;
		scoreText.enabled = false;

		// scoreUpdate function will be called after 1 second and it will
		// repeat after every 0.5 second
		InvokeRepeating ("scoreUpdate", 1f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		scoreText.text = "Score: " + scoreNo;
	}

	public void scoreUpdate() {
		// Application.loadlevel diyechi jate first scene e score text na dekhay
		if (gameOver == false && Application.loadedLevel != 0) {
			scoreText.enabled = true;
			scoreNo +=1;
		}
	}

	// game over holei button dekhabe 
	public void gameOverActivated() {
		gameOver = true;

		foreach (Button button in buttons) {
			button.gameObject.SetActive(true);
		}

	}

	// for paly button 
	public void play() {
		Application.LoadLevel (1);
	}


	// pause the game
	public void pause() {
		if (Time.timeScale == 0) {
			Time.timeScale = 1;
		}else if (Time.timeScale == 1) {
			Time.timeScale = 0;
		}	
	}

	// exit from game
	public void exit() {
		Application.Quit ();
	}

	// to go to the start menu of the game
	public void menu() {
		Application.LoadLevel (0);
	}

}
