﻿using UnityEngine;
using System.Collections;

public class PlayerCarController : MonoBehaviour {

	// player car speed
	public float speed;
	// car position
	Vector3 position;
	// to get the control over UI manager class (inheritance)
	public UiManager uiManager;

	// variable for accelerometer input
	float acclY,acclx;

	// Use this for initialization
	void Start () {
		// Initial position of the player
		position = transform.position;


		// get the accelerometer position of the player car
		//acclx = Input.acceleration.x;
		acclY = Input.acceleration.y;
	}
	
	// Update is called once per frame
	void Update () {

		// control for the Desktop device
		if (SystemInfo.deviceType == DeviceType.Desktop) {

			// get the x axis
			position.x += Input.GetAxis ("Horizontal") * speed * Time.deltaTime;
			
			// get the y axis
			position.y += Input.GetAxis ("Vertical") * speed * Time.deltaTime;

			// Mathf.clamp is used for remain the position into a certain boundary 

			position.x = Mathf.Clamp (position.x, -2.32f, 2.32f);
//			position.y = Mathf.Clamp (position.y, -3.8f, 3.8f);

			transform.position = position;
		} else {
			// get the input of x axis
			float x = Input.acceleration.x;
			// minus the previous accelerometer position so that car doesn't
			// move up autometicallu 
			float y = Input.acceleration.y - acclY;

			// pass the postion
			Vector2 move = new Vector2(x,0);


			if (move.sqrMagnitude > 1)
				move.Normalize();

			Vector2 pos = transform.position;

			pos += move * speed * Time.deltaTime;

			// To stay in the scene position
			pos.x = Mathf.Clamp (pos.x, -2.32f, 2.32f);

			transform.position = pos;

		}
	}


	// if car collide with enemy car
	void OnCollisionEnter2D(Collision2D col){
		if (col.gameObject.tag == "Enemy Car") {
			// car object will destroy
			Destroy(gameObject);

			// game over menu will be called
			uiManager.gameOverActivated();

		}
	}

}
