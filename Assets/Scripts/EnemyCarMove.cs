﻿using UnityEngine;
using System.Collections;

public class EnemyCarMove : MonoBehaviour {

	// defining enemy car speed
	public float speed = 5f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		// move the enemy car object at the Y axis 
		// adding Time.deltaTime for changing position on every frame

		transform.Translate (new Vector3(0, 1, 0) * speed * Time.deltaTime);
	}
}
