﻿using UnityEngine;
using System.Collections;

public class MoveTrack : MonoBehaviour {

	// defining the track/street speed
	public float speed;
	Vector2 offset;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		// defining move of the track for 2D 

		offset = new Vector2 (0, Time.time * speed);
		// Getting the component of the renderer and adding the offset to the texure rendered for the movement
		GetComponent<Renderer> ().material.mainTextureOffset = offset;
	}
}
